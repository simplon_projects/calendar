import moment from "moment";

let m = moment();

let header = document.querySelector('#header');
header.innerHTML = m.format('MMMM YY');

let daysInMonth = m.daysInMonth();

let arrDaysInvert = [];
let arrDays = [];
while (daysInMonth) {
    arrDaysInvert.push(daysInMonth);
    daysInMonth--;
}
arrDays = Array.prototype.reverse.call(arrDaysInvert);
console.log(arrDaysInvert);
console.log(arrDays);


let firstDayOfMonth = m.startOf('month').format('dddd');
console.log(firstDayOfMonth);

let arrCells = document.querySelectorAll('td');

for (let i = 0; i < arrDays.length; i++) {
    switch (firstDayOfMonth) {
        case 'Sunday':
            arrCells[i].innerHTML = arrDays[i];
            break;
        case 'Monday':
            arrCells[i + 1].innerHTML = arrDays[i];
            break;
        case 'Tuesday':
            arrCells[i + 2].innerHTML = arrDays[i];
            break;
        case 'Wednesday':
            arrCells[i + 3].innerHTML = arrDays[i];
            break;
        case 'Thursday':
            arrCells[i + 4].innerHTML = arrDays[i];
            break;
        case 'Friday':
            arrCells[i + 5].innerHTML = arrDays[i];
            break;
        case 'Saturday':
            arrCells[i + 6].innerHTML = arrDays[i];
            break;
    }
}